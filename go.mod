module gitlab.com/eemj/auto-passthrough-libvirt

go 1.17

require (
	github.com/digitalocean/go-libvirt v0.0.0-20210723161134-761cfeeb5968
	github.com/pilebones/go-udev v0.0.0-20210126000448-a3c2a7a4afb7
	gitlab.com/eemj/anime-pack v0.0.0-20211116224310-5101a9ffec23
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.18.1 // indirect
)
