package main

import (
	"fmt"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/digitalocean/go-libvirt"
	"github.com/pilebones/go-udev/netlink"
	"gitlab.com/eemj/anime-pack/pkg/log"
	"go.uber.org/zap"
)

const name = "win10"

type Event uint8

const (
	Attach Event = 1
	Detach Event = 2
)

func (e Event) String() string {
	switch e {
	case Attach:
		return "ATTACH"
	case Detach:
		return "DETACH"
	}

	return ""
}

type Device struct {
	Path          string `json:"path"`
	SubSystem     string `json:"subSystem"`
	Event         Event  `json:"event"`
	VendorID      string `json:"vendorID"`
	ProductID     string `json:"productID"`
	BusAddress    string `json:"busAddress"`
	DeviceAddress string `json:"deviceAddress"`
}

func (d Device) String() string {
	return fmt.Sprintf("[%s] (%s): %s", d.Event, d.SubSystem, d.Path)
}

func (d Device) HostDev() string {
	sb := new(strings.Builder)

	sb.WriteString(`<hostdev mode='subsystem' type='usb'>
	  <source>`)

	if len(strings.TrimSpace(d.VendorID)) > 0 {
		sb.WriteRune('\n')
		sb.WriteString(fmt.Sprintf(`    <vendor id="%s"/>`, d.VendorID))
	}

	if len(strings.TrimSpace(d.ProductID)) > 0 {
		sb.WriteRune('\n')
		sb.WriteString(fmt.Sprintf(`    <product id="%s"/>`, d.ProductID))
	}

	sb.WriteString(fmt.Sprintf(
		`    <address bus="%s" device="%s"/>
	  </source>
	</hostdev>`,
		unpad(d.BusAddress),
		unpad(d.DeviceAddress),
	))

	return sb.String()
}

func unpad(phrase string) (out string) {
	out = phrase

	if len(phrase) <= 1 || phrase[0] != '0' {
		return
	}

	var (
		index int
		digit rune
	)

	for index, digit = range phrase {
		if digit != '0' {
			break
		}
	}

	out = phrase[index:]

	return
}

func containsKey(env map[string]string, keys ...string) bool {
	for _, key := range keys {
		if _, exists := env[key]; !exists {
			return false
		}
	}

	return true
}

func monitor() (quitc chan struct{}, devicec chan Device, errc chan error) {
	conn := &netlink.UEventConn{}
	errc = make(chan error)

	if err := conn.Connect(netlink.UdevEvent); err != nil {
		errc <- err
		return
	}

	devicec = make(chan Device, 1)
	eventc := make(chan netlink.UEvent)

	quitc = conn.Monitor(eventc, errc, nil)

	go func() {
		for {
			select {
			case <-quitc:
				conn.Close()
				return
			case uevent := <-eventc:
				event := Event(0)

				if uevent.Action == netlink.ADD {
					event = Attach
				} else if uevent.Action == netlink.REMOVE {
					event = Detach
				} else {
					break
				}

				if !containsKey(uevent.Env, "SUBSYSTEM", "PRODUCT", "BUSNUM", "DEVNUM") {
					break
				}

				subSystem := uevent.Env["SUBSYSTEM"]

				if subSystem != "usb" {
					break
				}

				products := strings.Split(uevent.Env["PRODUCT"], "/")

				devicec <- Device{
					Path:          uevent.KObj,
					SubSystem:     subSystem,
					Event:         event,
					VendorID:      fmt.Sprintf("0x%05s", products[0]),
					ProductID:     fmt.Sprintf("0x%05s", products[1]),
					BusAddress:    uevent.Env["BUSNUM"],
					DeviceAddress: uevent.Env["DEVNUM"],
				}
			}
		}
	}()

	return
}

func connect() (conn net.Conn, client *libvirt.Libvirt, err error) {
	conn, err = net.DialTimeout(
		"unix",
		"/var/run/libvirt/libvirt-sock",
		(5 * time.Second),
	)

	if err != nil {
		return
	}

	client = libvirt.New(conn)

	if err = client.Connect(); err != nil {
		return
	}

	return
}

func main() {
	conn, client, err := connect()

	if err != nil {
	}

	defer conn.Close()

	domain, err := client.DomainLookupByName(name)

	if err != nil {
		log.Fatal(err)
	}

	defer client.Disconnect()

	quit, devicec, errc := monitor()

	if err != nil {
		log.Fatal(err)
	}

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGKILL)

	for {
		select {
		case <-sig:
			close(quit)
			return
		case err := <-errc:
			log.Fatal(err)
		case device := <-devicec:
			hostdev := device.HostDev()
			err := (error)(nil)

			log.Infow(
				"Update",
				zap.Stringer("event", device.Event),
				zap.String("path", device.Path),
				zap.String("sub_system", device.SubSystem),
			)

			if device.Event == Attach {
				err = client.DomainAttachDevice(domain, hostdev)
			} else {
				err = client.DomainDetachDevice(domain, hostdev)
			}

			if err != nil {
				log.Errorw("Update", zap.Error(err))
			}
		}
	}
}
